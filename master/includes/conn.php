<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	$mysqlHost = "127.0.0.1";
	$mysqlUser = "root";
	$mysqlPass = "";
	$mysqlDB   = "test";
	$connection = mysqli_connect("{$mysqlHost}", "{$mysqlUser}", "{$mysqlPass}");
	if(!$connection) {
		error_log("Failed to connect to MySQL: " . mysqli_error($connection));
		die('Internal server error');
	}
	$db_select = mysqli_select_db($connection, $mysqlDB);

	if(!$db_select) {
		error_log("Database selection failed: " . mysqli_error($connection));
		die('Internal server error');
	}
	//mysqli_query("alter table users auto_increment = 1 'utf-8'");
	// mysqli_query("SET NAMES 'utf8'");
?>